module "common" {
  source = "git::ssh://git@gitlab.com:rplessl-terraform-aws-modules/common.git?ref=v1.0.0"
}

module "common-private" {
  source = "../common-private"
}
