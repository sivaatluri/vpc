# create special cloud-init file
data "template_file" "service-host" {
  count    = "${var.create_service_host ? 1 : 0}"
  template = "${module.common.cloud_init_service_host}"

  vars = {
    hostname = "${var.customer_name_aws}-${var.aws_region}-${var.env}-sh"
    fqdn     = "${var.customer_name_aws}-${var.aws_region}-${var.env}-sh.${var.customer_domain_name}"
  }
}

# create service host
resource "aws_instance" "service-host" {
  count                       = "${var.create_service_host ? 1 : 0}"
  ami                         = "${module.common.ubuntu_xenial_amd64_ami}"
  availability_zone           = "${module.common.availability_zones[count.index % length(module.common.availability_zones)]}"
  instance_type               = "t3.micro"
  key_name                    = "${var.aws_key_name}"
  subnet_id                   = "${element(aws_subnet.prunux-mgt-public.*.id, count.index % length(module.common.availability_zones))}"
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.service-host.name}"
  ebs_optimized               = true

  credit_specification = {
    cpu_credits = "standard"
  }

  root_block_device {
    delete_on_termination = false
    volume_type           = "gp2"
    volume_size           = "${var.service_host_disk_size}"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prunux_mgt.id}",
    "${aws_security_group.service-host.id}",
  ]

  tags = {
    Name        = "${var.customer_name_aws}-${var.aws_region}-${var.env}-sh"
    Environment = "${var.env}"
    VPC         = "${module.common.vpcs[var.env]}"
    Customer    = "${var.customer_name}"
    Customer_ID = "${var.customer_id}"
  }

  user_data = "${data.template_file.service-host.rendered}"

  lifecycle {
    ignore_changes = ["ami", "user_data"]
  }
}

### Setup sh.vpc.CUSTOMER_DOMAIN_NAME with lambda ###

# Auto setup sh.vpc.CUSTOMER_DOMAIN_NAME
resource "aws_lambda_function" "service-host_lambda" {
  count            = "${var.create_service_host ? 1 : 0}"
  function_name    = "service-host-${var.aws_region}-${var.env}_dns"
  runtime          = "python2.7"
  filename         = "${path.module}/lambda/sh_lambda.py.zip"
  role             = "${aws_iam_role.service-host_lambda.arn}"
  handler          = "sh_lambda.lambda_handler"
  source_code_hash = "${base64sha256(file("${path.module}/lambda/sh_lambda.py.zip"))}"
  timeout          = 30

  environment = {
    variables = {
      VPC = "${module.common.vpcs[var.env]}"
    }
  }

  /*
    ignore changes to filename as path.module resolves to a absolute
    path on the local system. Remove temporarily when updating the
    lambda function (https://github.com/hashicorp/terraform/issues/8204)
    last_modified is also buggy:
      https://github.com/terraform-providers/terraform-provider-aws/issues/3630
  */

  lifecycle {
    ignore_changes = ["filename", "last_modified"]
  }
}

resource "aws_lambda_permission" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  statement_id  = "AWSEvents_to_ServiceHost_Lambda"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.service-host_lambda.arn}"
  principal     = "events.amazonaws.com"
}

resource "aws_iam_role" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  name = "service-host-${var.aws_region}-${var.env}_lambda_role"

  assume_role_policy = <<EOF
{
   "Statement" : [
      {
         "Principal" : {
            "Service" : "lambda.amazonaws.com"
         },
         "Effect" : "Allow",
         "Action" : "sts:AssumeRole"
      }
   ],
   "Version" : "2012-10-17"
}
EOF
}

resource "aws_iam_policy" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  name        = "service-host-${var.aws_region}-${var.env}_lambda_policy"
  path        = "/"
  description = "Lambda Policy for service-host lambda"

  policy = <<EOF
{
   "Version" : "2012-10-17",
   "Statement" : [
      {
         "Action" : [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
         ],
         "Resource" : "*",
         "Effect" : "Allow"
      },
      {
        "Action" : "ec2:Describe*",
        "Resource" : "*",
        "Effect" : "Allow"
      },
      {
         "Action" : [
            "route53:ChangeResourceRecordSets"
         ],
         "Resource" : [
            "arn:aws:route53:::hostedzone/${aws_route53_zone.vpc_prunux_net.zone_id}"
         ],
         "Effect" : "Allow"
      },
      {
         "Action" : [
            "route53:ListHostedZones",
            "route53:ListResourceRecordSets",
            "route53:ListTagsForResource"
         ],
         "Resource" : [
            "*"
         ],
         "Effect" : "Allow"
      }
   ]
}
EOF
}

resource "aws_iam_policy_attachment" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  name       = "lambda"
  roles      = ["${aws_iam_role.service-host_lambda.name}"]
  policy_arn = "${aws_iam_policy.service-host_lambda.arn}"
}

resource "aws_cloudwatch_event_rule" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  name        = "ec2_events_service-host-${var.env}_lambda"
  description = "Capture EC2 events of the Service Host and invoke Lambda"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.ec2"
  ],
  "detail-type": [
    "EC2 Instance State-change Notification"
  ],
  "detail": {
    "state": [
      "running"
    ],
    "instance-id": [
      "${aws_instance.service-host.id}"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "service-host_lambda" {
  count = "${var.create_service_host ? 1 : 0}"

  rule = "${aws_cloudwatch_event_rule.service-host_lambda.name}"
  arn  = "${aws_lambda_function.service-host_lambda.arn}"
}
