# instance profile for all ec2 instances
resource "aws_iam_instance_profile" "common" {
  name = "instance-profile-${var.aws_region}-${module.common.vpcs[var.env]}"
  role = "${aws_iam_role.ec2_assume_role.name}"

  # the instance_profile is not created instantly, so we have to wait
  # this should be fixed according to https://github.com/hashicorp/terraform/issues/9474
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

# role to be used and assumed
resource "aws_iam_role" "ec2_assume_role" {
  name = "ec2-assume-role-${var.aws_region}-${module.common.vpcs[var.env]}"

  assume_role_policy = <<EOF
{
  "Statement" : [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ],
  "Version" : "2012-10-17"
}
EOF
}

# role-policies, specify more here
# policy for reading ec2 tags
resource "aws_iam_policy" "read_tags_policy" {
  name = "read-tags-policy-${var.aws_region}-${module.common.vpcs[var.env]}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeTags"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy" "read_cloudwatch_policy" {
  name = "read-cloudwatch-policy-${var.aws_region}-${module.common.vpcs[var.env]}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement":[{
      "Effect":"Allow",
      "Action":[
        "cloudwatch:GetMetricStatistics",
        "cloudwatch:ListMetrics"
      ],
      "Resource":"*",
      "Condition":{
         "Bool":{
            "aws:SecureTransport":"true"
            }
         }
      }
   ]
}
EOF
}

# attachments 
## all instances
resource "aws_iam_role_policy_attachment" "read_tags_attach_common" {
  role       = "${aws_iam_role.ec2_assume_role.id}"
  policy_arn = "${aws_iam_policy.read_tags_policy.arn}"
}
